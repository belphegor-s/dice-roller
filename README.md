# Dice Roller

A basic app which has a constarint layout, having a button and a TextView. The app uses vector arts for the dice and their XML form.

* Android Studio Version - `4.2.1`
* Gradle Build Version - `7.0`

> The `dice_roller.apk` is not signed. You can use any app (e.g **ApkSigner**) for this.
